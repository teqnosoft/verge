package verge;

import static java.lang.Integer.rotateLeft;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.security.spec.ECField;
import java.security.spec.ECFieldFp;
import java.security.spec.ECParameterSpec;
import java.security.spec.ECPoint;
import java.security.spec.ECPublicKeySpec;
import java.security.spec.EllipticCurve;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.nio.ByteBuffer;
import java.util.Arrays;

public class Wallet {

	public static byte[] makePrivateKeyFromEntropy(byte[] entropy) {
		byte[] privatekey = new byte[32];
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			md.update(entropy);
			privatekey = md.digest();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return privatekey;
	}
	
	public static String makeWalletImportFormatKey (byte[] privatekey) {
		byte[] checksum = new byte[4];
		final byte privatekeyprefix = (byte)0x9E;
		byte[] key = ByteBuffer.allocate(33).put(privatekeyprefix).put(privatekey).array();
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			byte[] round1 = md.digest(key);
			byte[] round2 = md.digest(round1);
			checksum = Arrays.copyOfRange(round2, 0, 4);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		byte[] wif = ByteBuffer.allocate(37).put(privatekeyprefix).put(privatekey).put(checksum).array();
		final String ALPHABET = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz";
		final BigInteger ALPHABET_SIZE = BigInteger.valueOf(ALPHABET.length());
		StringBuilder sb = new StringBuilder();
		BigInteger num = new BigInteger(1, wif);
		while (num.signum() != 0) {
			BigInteger[] quotrem = num.divideAndRemainder(ALPHABET_SIZE);
			sb.append(ALPHABET.charAt(quotrem[1].intValue()));
			num = quotrem[0];
		}
		for (int i = 0; i < wif.length && wif[i] == 0; i++)
			sb.append(ALPHABET.charAt(0));
		return sb.reverse().toString();
	}
	
	public static String makeCompressedWalletImportFormatKey (byte[] privatekey) {
		byte[] checksum = new byte[4];
		final byte privatekeyprefix = (byte)0x9E;
		final byte privatekeysuffix = (byte)0x01;
		byte[] key = ByteBuffer.allocate(34).put(privatekeyprefix).put(privatekey).put(privatekeysuffix).array();
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			byte[] round1 = md.digest(key);
			byte[] round2 = md.digest(round1);
			checksum = Arrays.copyOfRange(round2, 0, 4);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		byte[] cwif = ByteBuffer.allocate(38).put(key).put(checksum).array();
		final String ALPHABET = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz";
		final BigInteger ALPHABET_SIZE = BigInteger.valueOf(ALPHABET.length());
		StringBuilder sb = new StringBuilder();
		BigInteger num = new BigInteger(1, cwif);
		while (num.signum() != 0) {
			BigInteger[] quotrem = num.divideAndRemainder(ALPHABET_SIZE);
			sb.append(ALPHABET.charAt(quotrem[1].intValue()));
			num = quotrem[0];
		}
		for (int i = 0; i < cwif.length && cwif[i] == 0; i++)
			sb.append(ALPHABET.charAt(0));
		return sb.reverse().toString();
	}
	
	public static byte[] makePrivateKeyFromWalletImportFormatKey(String walletimportformatkey) {
		final String ALPHABET = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz";
		final BigInteger ALPHABET_SIZE = BigInteger.valueOf(ALPHABET.length());
		BigInteger num = BigInteger.ZERO;
		for (int i = 0; i < walletimportformatkey.length(); i++) {
			num = num.multiply(ALPHABET_SIZE);
			int digit = ALPHABET.indexOf(walletimportformatkey.charAt(i));
			if (digit == -1)
				throw new IllegalArgumentException("Invalid character for Base58Check");
			num = num.add(BigInteger.valueOf(digit));
		}
		byte[] b = num.toByteArray();
		if (b[0] == 0) b = Arrays.copyOfRange(b, 1, b.length);
		try {
			ByteArrayOutputStream buf = new ByteArrayOutputStream();
			for (int i = 0; i < walletimportformatkey.length() && walletimportformatkey.charAt(i) == ALPHABET.charAt(0); i++)
				buf.write(0);
			buf.write(b);
			byte[] wifbytes = buf.toByteArray();
			byte[] wif = Arrays.copyOfRange(wifbytes, 1, 33);
			return wif;
		} catch (IOException e) {
			throw new AssertionError(e);
		}
	}
	
	public static byte[] makePrivateKeyFromCompressedWalletImportFormatKey(String compressedwalletimportformatkey) {
		final String ALPHABET = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz";
		final BigInteger ALPHABET_SIZE = BigInteger.valueOf(ALPHABET.length());
		BigInteger num = BigInteger.ZERO;
		for (int i = 0; i < compressedwalletimportformatkey.length(); i++) {
			num = num.multiply(ALPHABET_SIZE);
			int digit = ALPHABET.indexOf(compressedwalletimportformatkey.charAt(i));
			if (digit == -1)
				throw new IllegalArgumentException("Invalid character for Base58Check");
			num = num.add(BigInteger.valueOf(digit));
		}
		byte[] b = num.toByteArray();
		if (b[0] == 0) b = Arrays.copyOfRange(b, 1, b.length);
		try {
			ByteArrayOutputStream buf = new ByteArrayOutputStream();
			for (int i = 0; i < compressedwalletimportformatkey.length() && compressedwalletimportformatkey.charAt(i) == ALPHABET.charAt(0); i++)
				buf.write(0);
			buf.write(b);
			byte[] cwifbytes = buf.toByteArray();
			byte[] cwif = Arrays.copyOfRange(cwifbytes, 1, 33);
			return cwif;
		} catch (IOException e) {
			throw new AssertionError(e);
		}
	}
	
	public static byte[] makePublicKeyFromPrivateKey (byte[] privatekey) {
		// Credit for the coding behind this function should go to dave_thompson_085 and SkateScout on stackoverflow for answering my elliptical curve questions
		byte[] publickey = new byte[65];
		String infinitypoint = "303E020100301006072A8648CE3D020106052B8104000A042730250201010420";
		int len = infinitypoint.length();
	    byte[] data = new byte[len / 2];
	    for (int i = 0; i < len; i += 2) {
	        data[i / 2] = (byte) ((Character.digit(infinitypoint.charAt(i), 16) << 4)
	                             + Character.digit(infinitypoint.charAt(i+1), 16));
	    }
		byte[] pkcs8_ec_secp256k1_prefix = data;
		byte[] pkcs8_privatekey = ByteBuffer.allocate(64).put(pkcs8_ec_secp256k1_prefix).put(privatekey).array();
		try {
			KeyFactory kf = KeyFactory.getInstance("EC");
			ECPrivateKey pk = (ECPrivateKey)kf.generatePrivate(new PKCS8EncodedKeySpec(pkcs8_privatekey));
		    final ECParameterSpec params = pk.getParams();
		    final EllipticCurve curve = params.getCurve();
		    final ECPoint g = pk.getParams().getGenerator();
		    final BigInteger kin = pk.getS();
		    final ECField field = curve.getField();
		    if(!(field instanceof ECFieldFp)) throw new UnsupportedOperationException(field.getClass().getCanonicalName());
		    final BigInteger p = ((ECFieldFp)field).getP();
		    final BigInteger a = curve.getA();
		    ECPoint R = ECPoint.POINT_INFINITY;
		    BigInteger k = kin.mod(p);
		    final int length = k.bitLength();
		    final byte[] binarray = new byte[length];
		    for(int i=0;i<=length-1;i++){
		        binarray[i] = k.mod(new BigInteger("2")).byteValue();
		        k = k.shiftRight(1);
		    }
		    for(int i = length-1;i >= 0;i--){
		    	if (!(R.equals(ECPoint.POINT_INFINITY))) {
		    		BigInteger slope = (R.getAffineX().pow(2)).multiply(new BigInteger("3"));
		    	    slope = slope.add(a);
		    	    slope = slope.multiply((R.getAffineY().multiply(new BigInteger("2"))).modInverse(p));
		    	    final BigInteger Xout = slope.pow(2).subtract(R.getAffineX().multiply(new BigInteger("2"))).mod(p);
		    	    final BigInteger Yout = (R.getAffineY().negate()).add(slope.multiply(R.getAffineX().subtract(Xout))).mod(p);
		    		R = new ECPoint(Xout, Yout);
		    	}
		        if(binarray[i]== 1) {
		        	if (R.equals(ECPoint.POINT_INFINITY)) {
		        		R = g;
		        	} else if (R==g || R.equals(g)) {
		        		BigInteger slope = (R.getAffineX().pow(2)).multiply(new BigInteger("3"));
			    	    slope = slope.add(a);
			    	    slope = slope.multiply((R.getAffineY().multiply(new BigInteger("2"))).modInverse(p));
			    	    final BigInteger Xout = slope.pow(2).subtract(R.getAffineX().multiply(new BigInteger("2"))).mod(p);
			    	    final BigInteger Yout = (R.getAffineY().negate()).add(slope.multiply(R.getAffineX().subtract(Xout))).mod(p);
			    		R = new ECPoint(Xout, Yout);
		        	} else if (!(g.equals(ECPoint.POINT_INFINITY))) {
			        	final BigInteger gX    = g.getAffineX();
			    	    final BigInteger sY    = g.getAffineY();
			    	    final BigInteger rX    = R.getAffineX();
			    	    final BigInteger rY    = R.getAffineY();
			    	    final BigInteger slope = (rY.subtract(sY)).multiply(rX.subtract(gX).modInverse(p)).mod(p);
			    	    final BigInteger Xout  = (slope.modPow(new BigInteger("2"), p).subtract(rX)).subtract(gX).mod(p);
			    	    BigInteger Yout =   sY.negate().mod(p);
			    	    Yout = Yout.add(slope.multiply(gX.subtract(Xout))).mod(p);
			        	R = new ECPoint(Xout, Yout);
		        	}
		        }
		    }
		    final ECPoint w = R;
		    final KeyFactory kg = KeyFactory.getInstance("EC");
		    ECPublicKey pubkey = (ECPublicKey)kg.generatePublic (new ECPublicKeySpec (w, params));
		    byte[] x = pubkey.getW().getAffineX().toByteArray();
		    byte[] y = pubkey.getW().getAffineY().toByteArray();
		    final byte publickeyprefix = (byte)0x04;
		    publickey = ByteBuffer.allocate(65).put(publickeyprefix).put(x,x.length-32,32).put(y,y.length-32,32).array();
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			e.printStackTrace();
		}
	    return publickey;
	}

	public static byte[] makeCompressedPublicKeyFromPrivateKey (byte[] privatekey) {
		byte[] publickey = new byte[65];
		String infinitypoint = "303E020100301006072A8648CE3D020106052B8104000A042730250201010420";
		int len = infinitypoint.length();
	    byte[] data = new byte[len / 2];
	    for (int i = 0; i < len; i += 2) {
	        data[i / 2] = (byte) ((Character.digit(infinitypoint.charAt(i), 16) << 4)
	                             + Character.digit(infinitypoint.charAt(i+1), 16));
	    }
		byte[] pkcs8_ec_secp256k1_prefix = data;
		byte[] pkcs8_privatekey = ByteBuffer.allocate(64).put(pkcs8_ec_secp256k1_prefix).put(privatekey).array();
		try {
			KeyFactory kf = KeyFactory.getInstance("EC");
			ECPrivateKey pk = (ECPrivateKey)kf.generatePrivate(new PKCS8EncodedKeySpec(pkcs8_privatekey));
		    final ECParameterSpec params = pk.getParams();
		    final EllipticCurve curve = params.getCurve();
		    final ECPoint g = pk.getParams().getGenerator();
		    final BigInteger kin = pk.getS();
		    final ECField field = curve.getField();
		    if(!(field instanceof ECFieldFp)) throw new UnsupportedOperationException(field.getClass().getCanonicalName());
		    final BigInteger p = ((ECFieldFp)field).getP();
		    final BigInteger a = curve.getA();
		    ECPoint R = ECPoint.POINT_INFINITY;
		    BigInteger k = kin.mod(p);
		    final int length = k.bitLength();
		    final byte[] binarray = new byte[length];
		    for(int i=0;i<=length-1;i++){
		        binarray[i] = k.mod(new BigInteger("2")).byteValue();
		        k = k.shiftRight(1);
		    }
		    for(int i = length-1;i >= 0;i--){
		    	if (!(R.equals(ECPoint.POINT_INFINITY))) {
		    		BigInteger slope = (R.getAffineX().pow(2)).multiply(new BigInteger("3"));
		    	    slope = slope.add(a);
		    	    slope = slope.multiply((R.getAffineY().multiply(new BigInteger("2"))).modInverse(p));
		    	    final BigInteger Xout = slope.pow(2).subtract(R.getAffineX().multiply(new BigInteger("2"))).mod(p);
		    	    final BigInteger Yout = (R.getAffineY().negate()).add(slope.multiply(R.getAffineX().subtract(Xout))).mod(p);
		    		R = new ECPoint(Xout, Yout);
		    	}
		        if(binarray[i]== 1) {
		        	if (R.equals(ECPoint.POINT_INFINITY)) {
		        		R = g;
		        	} else if (R==g || R.equals(g)) {
		        		BigInteger slope = (R.getAffineX().pow(2)).multiply(new BigInteger("3"));
			    	    slope = slope.add(a);
			    	    slope = slope.multiply((R.getAffineY().multiply(new BigInteger("2"))).modInverse(p));
			    	    final BigInteger Xout = slope.pow(2).subtract(R.getAffineX().multiply(new BigInteger("2"))).mod(p);
			    	    final BigInteger Yout = (R.getAffineY().negate()).add(slope.multiply(R.getAffineX().subtract(Xout))).mod(p);
			    		R = new ECPoint(Xout, Yout);
		        	} else if (!(g.equals(ECPoint.POINT_INFINITY))) {
			        	final BigInteger gX    = g.getAffineX();
			    	    final BigInteger sY    = g.getAffineY();
			    	    final BigInteger rX    = R.getAffineX();
			    	    final BigInteger rY    = R.getAffineY();
			    	    final BigInteger slope = (rY.subtract(sY)).multiply(rX.subtract(gX).modInverse(p)).mod(p);
			    	    final BigInteger Xout  = (slope.modPow(new BigInteger("2"), p).subtract(rX)).subtract(gX).mod(p);
			    	    BigInteger Yout =   sY.negate().mod(p);
			    	    Yout = Yout.add(slope.multiply(gX.subtract(Xout))).mod(p);
			        	R = new ECPoint(Xout, Yout);
		        	}
		        }
		    }
		    final ECPoint w = R;
		    final KeyFactory kg = KeyFactory.getInstance("EC");
		    ECPublicKey pubkey = (ECPublicKey)kg.generatePublic (new ECPublicKeySpec (w, params));
		    byte[] x = pubkey.getW().getAffineX().toByteArray();
		    BigInteger y = pubkey.getW().getAffineY();
		    if(y.mod(new BigInteger("2")).equals(BigInteger.ZERO)) {
		    	publickey = ByteBuffer.allocate(33).put((byte)0x02).put(x,x.length-32,32).array();
		    } else {
		    	publickey = ByteBuffer.allocate(33).put((byte)0x03).put(x,x.length-32,32).array();
		    }
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			e.printStackTrace();
		}
	    return publickey;
	}

	public static String makeAddress (byte[] publickey) {
		byte[] checksum = new byte[4];
		final byte networkprefix = (byte)0x1e;
		byte[] sha256keyhash = new byte[32];
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			sha256keyhash = md.digest(publickey);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		// The following code performs native-java RIPEMD-160 encryption on the SHA-256 public key hash and should be credited to Nayuki.
		final int BLOCK_LEN = 64;
		final int[] KL = {0x00000000, 0x5A827999, 0x6ED9EBA1, 0x8F1BBCDC, 0xA953FD4E};  // Round constants for left line
		final int[] KR = {0x50A28BE6, 0x5C4DD124, 0x6D703EF3, 0x7A6D76E9, 0x00000000};  // Round constants for right line
		final int[] RL= { 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,7,4,13,1,10,6,15,3,12,0,9,5,2,14,11,8,3,10,14,4,9,15,8,1,2,7,0,6,13,11,5,12,1,9,11,10,0,8,12,4,13,3,7,15,14,5,6,2,4,0,5,9,7,12,2,10,14,1,3,8,11,6,15,13 }; // Message schedule for left line
		final int[] RR = { 5,14,7,0,9,2,11,4,13,6,15,8,1,10,3,12,6,11,3,7,0,13,5,10,14,15,8,12,4,9,1,2,15,5,1,3,7,14,6,9,11,8,12,2,10,0,4,13,8,6,4,1,3,11,15,0,5,12,2,13,9,7,10,14,12,15,10,4,1,5,8,7,6,2,13,14,0,3,9,11 }; // Message schedule for right line
		final int[] SL = { 11,14,15,12,5,8,7,9,11,13,14,15,6,7,9,8,7,6,8,13,11,9,7,15,7,12,15,9,11,7,13,12,11,13,6,7,14,9,13,15,14,8,13,6,5,12,7,5,11,12,14,15,14,15,9,8,9,14,5,6,8,6,5,12,9,15,5,11,6,8,13,12,5,12,13,14,11,8,5,6 }; // Left-rotation for left line
		final int[] SR = { 8,9,9,11,13,15,15,5,7,7,8,11,14,14,12,6,9,13,15,7,12,8,9,11,7,7,12,7,6,15,13,11,9,7,15,11,8,6,6,14,12,13,5,14,13,13,7,5,15,5,8,11,14,14,6,14,6,9,12,9,12,5,15,8,8,5,12,9,12,5,14,6,8,13,6,5,15,13,11,11 }; // Left-rotation for right line
		int[] state = {0x67452301, 0xEFCDAB89, 0x98BADCFE, 0x10325476, 0xC3D2E1F0};
		int off = sha256keyhash.length / BLOCK_LEN * BLOCK_LEN;
		if (off % BLOCK_LEN != 0) throw new IllegalArgumentException();
		for (int i = 0; i < off; i += BLOCK_LEN) {
			int[] schedule = new int[16]; 
			for (int j = 0; j < BLOCK_LEN; j++) schedule[j / 4] |= (sha256keyhash[i + j] & 0xFF) << (j % 4 * 8);
			int al = state[0], ar = state[0];
			int bl = state[1], br = state[1];
			int cl = state[2], cr = state[2];
			int dl = state[3], dr = state[3];
			int el = state[4], er = state[4];
			for (int j = 0; j < 80; j++) {
				int temp,f;
				if (j < 16) f = bl ^ cl ^ dl;
				else if (j < 32) f = (bl & cl) | (~bl & dl);
				else if (j < 48) f = (bl | ~cl) ^ dl;
				else if (j < 64) f = (bl & dl) | (cl & ~dl);
				else f = bl ^ (cl | ~dl);
				temp = rotateLeft(al + f + schedule[RL[j]] + KL[j / 16], SL[j]) + el;
				al = el;
				el = dl;
				dl = rotateLeft(cl, 10);
				cl = bl;
				bl = temp;
				if ((79-j) < 16) f = br ^ cr ^ dr;
				else if ((79-j) < 32) f = (br & cr) | (~br & dr);
				else if ((79-j) < 48) f = (br | ~cr) ^ dr;
				else if ((79-j) < 64) f = (br & dr) | (cr & ~dr);
				else f = br ^ (cr | ~dr);
				temp = rotateLeft(ar + f + schedule[RR[j]] + KR[j / 16], SR[j]) + er;
				ar = er;
				er = dr;
				dr = rotateLeft(cr, 10);
				cr = br;
				br = temp;
			}
			int temp = state[1] + cl + dr;
			state[1] = state[2] + dl + er;
			state[2] = state[3] + el + ar;
			state[3] = state[4] + al + br;
			state[4] = state[0] + bl + cr;
			state[0] = temp;
		}
		byte[] block = new byte[BLOCK_LEN];
		System.arraycopy(sha256keyhash, off, block, 0, sha256keyhash.length - off);
		off = sha256keyhash.length % block.length;
		block[off] = (byte)0x80;
		off++;
		if (off + 8 > block.length) {
			if (block.length % BLOCK_LEN != 0) throw new IllegalArgumentException();
			for (int i = 0; i < block.length; i += BLOCK_LEN) {
				int[] schedule = new int[16]; 
				for (int j = 0; j < BLOCK_LEN; j++) schedule[j / 4] |= (block[i + j] & 0xFF) << (j % 4 * 8);
				int al = state[0], ar = state[0];
				int bl = state[1], br = state[1];
				int cl = state[2], cr = state[2];
				int dl = state[3], dr = state[3];
				int el = state[4], er = state[4];
				for (int j = 0; j < 80; j++) {
					int temp,f;
					if (j < 16) f = bl ^ cl ^ dl;
					else if (j < 32) f = (bl & cl) | (~bl & dl);
					else if (j < 48) f = (bl | ~cl) ^ dl;
					else if (j < 64) f = (bl & dl) | (cl & ~dl);
					else f = bl ^ (cl | ~dl);
					temp = rotateLeft(al + f + schedule[RL[j]] + KL[j / 16], SL[j]) + el;
					al = el;
					el = dl;
					dl = rotateLeft(cl, 10);
					cl = bl;
					bl = temp;
					if ((79-j) < 16) f = br ^ cr ^ dr;
					else if ((79-j) < 32) f = (br & cr) | (~br & dr);
					else if ((79-j) < 48) f = (br | ~cr) ^ dr;
					else if ((79-j) < 64) f = (br & dr) | (cr & ~dr);
					else f = br ^ (cr | ~dr);
					temp = rotateLeft(ar + f + schedule[RR[j]] + KR[j / 16], SR[j]) + er;
					ar = er;
					er = dr;
					dr = rotateLeft(cr, 10);
					cr = br;
					br = temp;
				}
				int temp = state[1] + cl + dr;
				state[1] = state[2] + dl + er;
				state[2] = state[3] + el + ar;
				state[3] = state[4] + al + br;
				state[4] = state[0] + bl + cr;
				state[0] = temp;
			}
			Arrays.fill(block, (byte)0);
		}
		long len = (long)sha256keyhash.length << 3;
		for (int i = 0; i < 8; i++) block[block.length - 8 + i] = (byte)(len >>> (i * 8));
		if (block.length % BLOCK_LEN != 0) throw new IllegalArgumentException();
		for (int i = 0; i < block.length; i += BLOCK_LEN) {
			int[] schedule = new int[16]; 
			for (int j = 0; j < BLOCK_LEN; j++) schedule[j / 4] |= (block[i + j] & 0xFF) << (j % 4 * 8);
			int al = state[0], ar = state[0];
			int bl = state[1], br = state[1];
			int cl = state[2], cr = state[2];
			int dl = state[3], dr = state[3];
			int el = state[4], er = state[4];
			for (int j = 0; j < 80; j++) {
				int temp,f;
				if (j < 16) f = bl ^ cl ^ dl;
				else if (j < 32) f = (bl & cl) | (~bl & dl);
				else if (j < 48) f = (bl | ~cl) ^ dl;
				else if (j < 64) f = (bl & dl) | (cl & ~dl);
				else f = bl ^ (cl | ~dl);
				temp = rotateLeft(al + f + schedule[RL[j]] + KL[j / 16], SL[j]) + el;
				al = el;
				el = dl;
				dl = rotateLeft(cl, 10);
				cl = bl;
				bl = temp;
				if ((79-j) < 16) f = br ^ cr ^ dr;
				else if ((79-j) < 32) f = (br & cr) | (~br & dr);
				else if ((79-j) < 48) f = (br | ~cr) ^ dr;
				else if ((79-j) < 64) f = (br & dr) | (cr & ~dr);
				else f = br ^ (cr | ~dr);
				temp = rotateLeft(ar + f + schedule[RR[j]] + KR[j / 16], SR[j]) + er;
				ar = er;
				er = dr;
				dr = rotateLeft(cr, 10);
				cr = br;
				br = temp;
			}
			int temp = state[1] + cl + dr;
			state[1] = state[2] + dl + er;
			state[2] = state[3] + el + ar;
			state[3] = state[4] + al + br;
			state[4] = state[0] + bl + cr;
			state[0] = temp;
		}
		byte[] ripemd160keyhash = new byte[state.length * 4];
		for (int i = 0; i < ripemd160keyhash.length; i++) ripemd160keyhash[i] = (byte)(state[i / 4] >>> (i % 4 * 8));
		// End of RIPEMD-160 encryption
		
		byte[] keyhash = ByteBuffer.allocate(21).put(networkprefix).put(ripemd160keyhash).array();
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			byte[] round1 = md.digest(keyhash);
			byte[] round2 = md.digest(round1);
			checksum = Arrays.copyOfRange(round2, 0, 4);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		byte[] addressbytes = ByteBuffer.allocate(25).put(keyhash).put(checksum).array();
		final String ALPHABET = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz";
		final BigInteger ALPHABET_SIZE = BigInteger.valueOf(ALPHABET.length());
		StringBuilder sb = new StringBuilder();
		BigInteger num = new BigInteger(1, addressbytes);
		while (num.signum() != 0) {
			BigInteger[] quotrem = num.divideAndRemainder(ALPHABET_SIZE);
			sb.append(ALPHABET.charAt(quotrem[1].intValue()));
			num = quotrem[0];
		}
		for (int i = 0; i < addressbytes.length && addressbytes[i] == 0; i++)
			sb.append(ALPHABET.charAt(0));
		return sb.reverse().toString();
	}

}
